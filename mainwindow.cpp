#include "mainwindow.h"
#include "ui_mainwindow.h"


Q_DECLARE_METATYPE(QCameraInfo)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_duration(0),
    FragmentDuration(10),
    player(new MyVlcPlayer(media_data, this)),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf8"));

    media_data.videoWidget = ui->videoWidget;

    indexRead();

    initDevices();
    initUI();
    init_media_data();
    player->startCamera();
    timer = new QTimer(this);
    connect(timer,      &QTimer::timeout,   this,   &MainWindow::updateInterface);
}


MainWindow::~MainWindow()   {
    delete ui;
    delete player;
}



void MainWindow::initDevices()  {
    QActionGroup *videoDevicesGroup = new QActionGroup(this);
    videoDevicesGroup->setExclusive(true);
    const QList<QCameraInfo> availableCameras = QCameraInfo::availableCameras();
    for (const QCameraInfo &cameraInfo : availableCameras) {
        QAction *videoDeviceAction = new QAction(cameraInfo.description(), videoDevicesGroup);
        videoDeviceAction->setCheckable(true);
        videoDeviceAction->setData(QVariant::fromValue(cameraInfo));
        if (cameraInfo == QCameraInfo::defaultCamera()) {
            videoDeviceAction->setChecked(true);
            media_data.videoDeviceName = cameraInfo.description();
            media_data.cameraInfo = cameraInfo;
        }
        ui->menuDevices->addAction(videoDeviceAction);
    }

    ui->menuDevices->addSeparator();

    QActionGroup *audioDevicesGroup = new QActionGroup(this);
    audioDevicesGroup->setExclusive(true);
    const QList<QAudioDeviceInfo> availableAudioInputs = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
    for(const QAudioDeviceInfo &audioInfo : availableAudioInputs)    {
        QAction *audioDeviceAction = new QAction(audioInfo.deviceName(), audioDevicesGroup);
        audioDeviceAction->setCheckable(true);
        audioDeviceAction->setData(QVariant::fromValue(audioInfo));
        if (audioInfo == QAudioDeviceInfo::defaultInputDevice())    {
            audioDeviceAction->setChecked(true);
            media_data.audioDeviceName = audioInfo.deviceName();
        }
        ui->menuDevices->addAction(audioDeviceAction);
    }

//    connect(videoDevicesGroup,  &QActionGroup::triggered,   [=] () {player->setMedia(media_data);}  );
//    connect(audioDevicesGroup,  &QActionGroup::triggered,   [=] () {player->setMedia(media_data);}  );

    connect(videoDevicesGroup, &QActionGroup::triggered, this, &MainWindow::updateCameraDevice);
    connect(audioDevicesGroup, &QActionGroup::triggered, this, &MainWindow::updateAudioDevice);

}

void MainWindow::init_media_data()
{
    //  ":sout=#transcode{vcodec=mp2v,vb=5000,scale=Auto,acodec=mp3,ab=320,channels=2,samplerate=48000,scodec=none}"
    //  ":file{mux=mp4,dst=xyz.%1.mp4}"
    //  ":duplicate{dst=file{mux=mp4,dst=xyz.%1.mp4},dst=display}"

    media_data.fileName     = "storage/data";
    media_data.fileExt      = "ogg";    //"mp4";
    media_data.container    = "ogg";    //"mp4";
    media_data.v_codec      = "theo";   // "mp2v";
    media_data.a_codec      = "vorb";   // "mp3";
    media_data.v_bitrate    = 1800;     //5000;
    media_data.a_bitrate    = 128;      //128;
    media_data.scale        = "Auto";
    media_data.channels     = 2;
    media_data.samplerate   = 48000;
    media_data.s_codec      = "none";

    media_data.isRecording  = false;
    media_data.isDisplaying = false;
}



void MainWindow::updateCameraDevice(QAction *action)
{
    media_data.videoDeviceName = qvariant_cast<QString>(action->data());
//    player->setMedia(media_data);
}


void MainWindow::updateAudioDevice(QAction *action)
{
    media_data.audioDeviceName = qvariant_cast<QString>(action->data());
//    player->setMedia(media_data);
}


void MainWindow::initUI() {
    /* Menu */
    QAction *Quit   = new QAction("&Quit", this);
    QAction *About  = new QAction("&About", this);
//    QAction *runCamera = new QAction("&Run", this);


    Quit->setShortcut(QKeySequence("Ctrl+Q"));
//    runCamera->setShortcut(QKeySequence("Ctrl+R"));

    ui->menuFile->addAction(About);
    ui->menuFile->addAction(Quit);
//    camMenu->addAction(runCamera);

    ui->lblWholeTime->setText("00:00");

    ui->btnRec->setCheckable(true);
    ui->btnView->setCheckable(true);

    connect(About,          &QAction::triggered,        this,   &MainWindow::about);
    connect(Quit,           &QAction::triggered,        this,   &MainWindow::quit);
    connect(ui->btnRec,     &QPushButton::toggled,      this,   &MainWindow::StartStop);
    connect(ui->btnView,    &QPushButton::toggled,      this,   &MainWindow::display_ON_OFF);
}



void MainWindow::StartStop(bool rec)    {
    media_data.isRecording = rec;
    if (rec) {
        media_data.cntFiles++;
        player->restartCamera();
        m_timeStart = QTime::currentTime();
        timer->start(1000);
    }   else    {
        timer->stop();
        player->restartCamera();
        indexWrite();
    }
}


void MainWindow::display_ON_OFF(bool displayed)   {
    media_data.isDisplaying = displayed;

    if (media_data.isRecording) {
        indexWrite();
        media_data.cntFiles++;
    }

    player->restartCamera();

    m_timeStart = QTime::currentTime();
}



void MainWindow::updateInterface() { //Update interface every 1 sec
    if (media_data.isDisplaying) ui->btnView->setText("Display OFF");
    else    ui->btnView->setText("Display ON");

    if (media_data.isRecording) ui->btnRec->setText("Record STOP");
    else    ui->btnRec->setText("Record START");

    /* update the timeline */
    m_duration = player->getDuration() / 1000;          // turn ms into sec

    QTime   totalTime((m_duration / 3600) % 60, (m_duration / 60) % 60, m_duration % 60);
    QString format = "mm:ss";
    if (m_duration > 3600)
        format = "hh:mm:ss";

    ui->lblWholeTime->setText(totalTime.toString(format));

    if (m_duration && !(m_duration % 10))   {
//        player->stopRecording();
        indexWrite();
        media_data.cntFiles++;
        player->restartCamera();
        m_timeStart = QTime::currentTime();
    }
}


void MainWindow::indexWrite() {
    //QTextCodec::setCodecForLocale(QTextCodec::codecForName("Windows-1251"));

    QFile   csvFile(QDir::currentPath() + "/storage/index.csv");
    QTime   timeStop = m_timeStart.addSecs(m_duration);

    if(csvFile.open( QIODevice::ReadWrite | QIODevice::Append ))    {
        QTextStream stream( &csvFile );
        stream << media_data.cntFiles << ";" << m_duration << ";" << m_timeStart.toString("hh:mm:ss") << ";"
               << timeStop.toString("hh:mm:ss") << '\n';

        csvFile.close();
    }
}


void MainWindow::indexRead() {
    //QTextCodec::setCodecForLocale(QTextCodec::codecForName("Windows-1251"));

    QFile csvFile(QDir::currentPath() + "/storage/index.csv");

    if(csvFile.open( QIODevice::ReadOnly ))    {
        QTextStream stream( &csvFile );
        qint64 durationTotal = 0;
        while (!stream.atEnd()) {
            QString line = stream.readLine();
            durationTotal += QString(line.split(";").at(1)).toInt();
            media_data.cntFiles = QString(line.split(";").at(0)).toInt();
        }
        csvFile.close();
    }   else
        media_data.cntFiles = 0;
}



void MainWindow::quit() {
    player->stopCamera();
    qApp->quit();
}




void MainWindow::about()
{
    QMessageBox::about(this, "My Qt/libVLC recorder demo", player->getVersion());
}

