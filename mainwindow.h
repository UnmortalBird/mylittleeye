#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QSlider>
#include <QtWidgets>
#include <QMessageBox>
#include <QTimer>
#include <QMenuBar>
#include <QAction>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QLabel>
#include <QTime>

#include <QAudioDeviceInfo>
#include <QTextCodec>

#include <QCameraInfo>
#include <QMediaMetaData>

#include <QDebug>

#include <myvlcplayer.h>

#include <media_struct.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
//    void mute();
    void about();
    void quit();
//    void fullscreen();

//    int changeVolume(int);

protected:
//    virtual void closeEvent(QCloseEvent*);


private:
    void                updateCameraDevice(QAction *action);
    void                updateAudioDevice(QAction *action);

    void                indexWrite();
    void                indexRead();

    void                initUI();
    void                initDevices();
    void                init_media_data();

    void                updateInterface();
    void                StartStop(bool rec);
    void                display_ON_OFF(bool display);

    QTime               m_timeStart;
    qint64              m_duration;
    //qint64              m_cntFiles;
    //bool                m_isDisplay;
//    bool                m_isRecord;

    //QString             videoDeviceName;
    //QString             audioDeviceName;

    int                 FragmentDuration;

    MyVlcPlayer         *player;

//    MyVlcPlayer         *player_2;
//    QThread             *thread;

    QTimer *timer;

    media_data_struct   media_data;
    Ui::MainWindow      *ui;
};

#endif // MAINWINDOW_H
