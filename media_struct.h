#ifndef MEDIA_STRUCT_H
#define MEDIA_STRUCT_H

#include <QWidget>
#include <QTime>
#include <QCameraInfo>

struct media_data_struct
{
    qint64      cntFiles;

    QCameraInfo cameraInfo;

    QString     videoDeviceName;
    QString     audioDeviceName;

    QString     fileName;
    QString     fileExt;
    QString     container;
    QString     v_codec;
    QString     a_codec;
    int         v_bitrate;
    int         a_bitrate;
    int         v_width;
    int         v_height;
    QString     scale;
    int         channels;
    int         samplerate;
    QString     s_codec;

    QWidget     *videoWidget;

    bool        isRecording;
    bool        isDisplaying;
};
//  ":sout=#transcode{vcodec=mp2v,vb=5000,scale=Auto,acodec=mp3,ab=320,channels=2,samplerate=48000,scodec=none}"
//  ":file{mux=mp4,dst=xyz.%1.mp4}"
//  ":duplicate{dst=file{mux=mp4,dst=xyz.%1.mp4},dst=display}"
#endif // MEDIA_STRUCT_H
