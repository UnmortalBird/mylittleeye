#include "myvlcplayer.h"


//static const char* vlcArguments[] = {
//    "--intf=dummy",
//    "--ignore-config",
//    "--no-media-library",
//    "--no-one-instance",
//    "--no-osd",
//    "--no-snapshot-preview",
//    "--no-stats",
//    "--no-video-title-show",
//    "-vvv"
//};

MyVlcPlayer::MyVlcPlayer(const media_data_struct &media_data, QWidget *parent) :
    QObject(parent),
    m_vlcInstance(libvlc_new(0, NULL)),
//    m_vlcInstance(libvlc_new(sizeof(vlcArguments) / sizeof(vlcArguments[0]), vlcArguments)),
    m_vlcPlayer(NULL),
    m_media_data(&media_data)
{
    /* Complain in case of broken installation */
//    if (m_vlcInstance == NULL) {
//        //QMessageBox::critical(this, "Qt libVLC player", "Could not init libVLC");
//        qDebug() << "could not init LibVLC";
//        exit(1);
//    }
}

MyVlcPlayer::~MyVlcPlayer() {
    libvlc_release(m_vlcInstance);
}


void MyVlcPlayer::startCamera()
{
    QString param;

    #if defined(Q_OS_LINUX)
        param = "v4l2://";
    #elif defined(Q_OS_WIN)
        param = "dshow://";
    #elif param(Q_OS_MAC)
        param = "qtcapture://";
    #else
    #error "unsupported platform"
    #endif

    libvlc_media_t * vlcMedia = libvlc_media_new_location(m_vlcInstance, param.toUtf8());


    if (m_media_data->isRecording)    {
        param = QString::fromUtf8(":dshow-vdev=%1").arg(m_media_data->videoDeviceName);
        libvlc_media_add_option(vlcMedia,   param.toLocal8Bit().constData());

        param = QString::fromUtf8(":dshow-adev=%1").arg(m_media_data->audioDeviceName);
        libvlc_media_add_option(vlcMedia,   param.toLocal8Bit().constData());

        param = QString(":sout=#transcode{vcodec=%1,vb=%2,scale=%3,acodec=%4,ab=%5,channels=%6,samplerate=%7,scodec=%8} ")
                    .arg(m_media_data->v_codec)
                    .arg(m_media_data->v_bitrate)
                    .arg(m_media_data->scale)
                    .arg(m_media_data->a_codec)
                    .arg(m_media_data->a_bitrate)
                    .arg(m_media_data->channels)
                    .arg(m_media_data->samplerate)
                    .arg(m_media_data->s_codec);

        QString fileParam = QString("file{mux=%1,dst=%2.%3.%4}")
                    .arg(m_media_data->container)
                    .arg(m_media_data->fileName)
                    .arg(m_media_data->cntFiles)
                    .arg(m_media_data->fileExt);

        if (m_media_data->isDisplaying)
            param = param + ":duplicate{dst=" + fileParam + ",dst=display}";
        else
            param = param + ':' + fileParam;
        libvlc_media_add_option(vlcMedia, param.toLocal8Bit().constData());
    }   else if (m_media_data->isDisplaying) {
            param = ":sout=#display";
            libvlc_media_add_option(vlcMedia, param.toLocal8Bit().constData());
    }

    m_vlcPlayer = libvlc_media_player_new_from_media(vlcMedia);
    libvlc_media_release(vlcMedia);


#if defined(Q_OS_LINUX)
    libvlc_media_player_set_xwindow(m_vlcPlayer, m_media_data->videoWidget->winId());
#elif defined(Q_OS_MAC)
    libvlc_media_player_set_nsobject(m_vlcPlayer, m_media_data->videoWidget->winId());
#elif defined(Q_OS_WIN)
    libvlc_media_player_set_hwnd(m_vlcPlayer, (HWND)m_media_data->videoWidget->winId());
#else
#error "unsupported platform"
#endif

    if (m_media_data->isDisplaying || m_media_data->isRecording)
        libvlc_media_player_play(m_vlcPlayer);
}




void MyVlcPlayer::stopCamera()
{
    if (m_vlcPlayer)    {

        if(libvlc_media_player_is_playing(m_vlcPlayer))    libvlc_media_player_stop(m_vlcPlayer);
        libvlc_media_player_release(m_vlcPlayer);
        m_vlcPlayer = 0;
    }
}


qint64 MyVlcPlayer::getDuration()  {            // in ms
    if(m_vlcPlayer) {
        return libvlc_media_player_get_time(m_vlcPlayer);
    }
    return 0;
}



void MyVlcPlayer::restartCamera()  {
    stopCamera();
    startCamera();
}



//void MyVlcPlayer::stopRecording()
//{
////    m_recording = false;
//    stopCamera();
//    startCamera();
//}



//void MyVlcPlayer::freeMemory()  {
//    if (vlcPlayer)
//        stopPlayer();

//    if (vlcInstance)
//        libvlc_release(vlcInstance);
//}



QString MyVlcPlayer::getVersion()   {
    return QString::fromUtf8(libvlc_get_version());
}
