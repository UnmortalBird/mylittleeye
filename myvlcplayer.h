#ifndef MYVLCPLAYER_H
#define MYVLCPLAYER_H

#include <QObject>
#include <QMessageBox>
#include <QFileDialog>

#include <vlc/vlc.h>

#include <QDebug>

#include "media_struct.h"

class MyVlcPlayer : public QObject
{
    Q_OBJECT

public:
    explicit    MyVlcPlayer(const media_data_struct &media_data, QWidget *parent = 0);
                ~MyVlcPlayer();

    qint64      getDuration();

//    void        setPosition(int pos);

    void        startCamera();
    void        stopCamera();
    void        restartCamera();

//    void        startRecording();
//    void        stopRecording();

    QString     getVersion();

private:
    libvlc_instance_t       *m_vlcInstance;
    libvlc_media_player_t   *m_vlcPlayer;

    const media_data_struct *m_media_data;
};

#endif // MYVLCPLAYER_H
